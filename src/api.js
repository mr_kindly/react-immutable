import axios from 'axios';

const productUrl = '/api/v1/products/';
const categoryUrl = '/api/v1/categories/';
const apiHost = 'https://remp-api.herokuapp.com';

export async function getProduct(id) {
  const product = await axios.get(`${apiHost}${productUrl}${id}`).then(res => res.data);
  return product;
}

export async function getCategoryProducts(id) {
  return axios.get(`${apiHost}${categoryUrl}${id}/products`).then(res => res.data);
}

export async function getCategory(id) {
  const category = await axios.get(`${apiHost}${categoryUrl}${id}`).then(res => res.data);
  return category;
}

export async function getCategories() {
  return await axios.get(`${apiHost}${categoryUrl}`).then(res => res.data);
}
